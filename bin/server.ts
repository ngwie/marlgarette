
/**
 * Module dependencies.
 */

import * as http from 'http';
import * as cluster from 'cluster';
import logger from '../lib/logger';

const IS_PRODUCTION = process.env.NODE_ENV == 'production';
const STOP_SIGNALS = [
  'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
  'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
];

let stopping = false;

cluster.on('disconnect', function(worker) {
  if (IS_PRODUCTION) {
    if (!stopping) {
      cluster.fork();
    }
  } else {
    process.exit(1);
  }
});

if (cluster.isMaster) {
  const workerCount = process.env.NODE_CLUSTER_WORKERS || 4;
  logger.info(`Starting ${workerCount} workers...`);
  for (let i = 0; i < workerCount; i++) {
    cluster.fork();
  }
  if (IS_PRODUCTION) {
    STOP_SIGNALS.forEach(function (signal) {
      process.on(signal, function () {
        logger.info(`Got ${signal}, stopping workers...`);
        stopping = true;
        cluster.disconnect(function () {
          logger.info('All workers stopped, exiting.');
          process.exit(0);
        });
      });
    });
  }
} else {
  require('./www');
}