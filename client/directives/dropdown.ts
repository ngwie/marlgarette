import { DropdownDirective } from './dropdown/dropdown.directive';
import { DropdownToggleDirective } from './dropdown/dropdown-toggle.directive';

export let DROPDOWN_DIRECTIVES: Array<any> = [ DropdownDirective, DropdownToggleDirective ];