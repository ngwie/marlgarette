import { Directive, ElementRef } from '@angular/core';
import { DropdownService } from './dropdown.service';

@Directive({
  selector: '[mrDropdown]',
  providers: [ DropdownService ]
})
export class DropdownDirective {
  private el: HTMLElement;
  private menuOpen = false;

  constructor(private dropdownService: DropdownService, el: ElementRef) {
    this.el = el.nativeElement;

    dropdownService.stateAnnounced$.subscribe((state) => {
      switch (state) {
        case 'toggle':
          this.menuOpen = !this.menuOpen;
          break;
        case 'close':
          this.menuOpen = false;
          break;
        case 'open':
          this.menuOpen = true;
        default:
          return;
      }

      let elContainOpen = this.el.classList.contains('open');

      if (this.menuOpen && !elContainOpen)
        return this.el.classList.add('open');

      if (!this.menuOpen && elContainOpen)
        this.el.classList.remove('open');
    });
  }

}