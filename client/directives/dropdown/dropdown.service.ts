import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class DropdownService {

  private stateAnnouncedSource = new Subject<string>();

  stateAnnounced$ = this.stateAnnouncedSource.asObservable();

  toggle(state: string) {
    this.stateAnnouncedSource.next(state);
  }

}