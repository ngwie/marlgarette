import { Directive, HostListener, OnDestroy } from '@angular/core';
import { DropdownService } from './dropdown.service';

@Directive({
  selector: '[mrDropdownToggle]',
})
export class DropdownToggleDirective implements OnDestroy {

  constructor(private dropdownService: DropdownService) {
    document.body.addEventListener('click', this.onClose.bind(this))
  }

  @HostListener('click', ['$event'])
  onClick(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.dropdownService.toggle('toggle');
  }

  ngOnDestroy() {
    document.body.removeEventListener('click', this.onClose.bind(this))
  }

  private onClose() {
    this.dropdownService.toggle('close');
  }

}