import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  trigger,
  state,
  style,
  transition,
  animate } from '@angular/core';

const STYLE_ANIMATION = '0.6s ease-in-out';
const ACTIVE_STYLE = style({ transform: 'translateX(0)' });
const INACTIVE_STYLE = style({ display: 'none' });

interface CarouselItem {
  url: string;
  state: string;
}

@Component({
  selector: 'carousel',
  templateUrl: 'carousel.component.html',
  styleUrls: [ 'carousel.component.css' ],
  animations: [
    trigger('slideState', [
      state('active', ACTIVE_STYLE),
      state('nextIn', ACTIVE_STYLE),
      state('prevIn', ACTIVE_STYLE),
      state('inactive', INACTIVE_STYLE),
      state('nextOut', INACTIVE_STYLE),
      state('prevOut', INACTIVE_STYLE),
      transition('* => nextIn', [
        style({ transform: 'translateX(-100%)', position: 'absolute', top: 0 }),
        animate(STYLE_ANIMATION)
      ]),
      transition('* => nextOut', [
        animate(STYLE_ANIMATION, style({ transform: 'translateX(100%)', position: 'absolute', top: 0 }))
      ]),
      transition('* => prevIn', [
        style({ transform: 'translateX(100%)', position: 'absolute', top: 0 }),
        animate(STYLE_ANIMATION)
      ]),
      transition('* => prevOut', [
        animate(STYLE_ANIMATION, style({ transform: 'translateX(-100%)', position: 'absolute', top: 0 }))
      ])
    ])
  ]
})
export class CarouselComponent implements OnInit, OnDestroy {
  cursor: number = 0;
  prevCursor: number;
  private moving: boolean;
  private interval: NodeJS.Timer;

  @Input()
  images: CarouselItem[];

  ngOnInit() {
    this.images.forEach((image) => { image.state = 'inactive'; });
    this.images[this.cursor].state = 'active';

    this.interval = setInterval(() => this.next(), 8000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  move(index: number, direction: string) {
    if (this.moving) return;
    this.moving = true;

    if (index === this.cursor ||
      index >= this.images.length ||
      index < 0) return;

    if (direction === 'right') {
      this.images[this.cursor].state = 'nextOut';
      this.images[index].state = 'nextIn';
    } else {
      this.images[this.cursor].state = 'prevOut';
      this.images[index].state = 'prevIn';
    }

    this.prevCursor = this.cursor;
    this.cursor = index;
  }

  next(event?) {
    if (event) event.preventDefault();

    this.move(this.cursor + 1 >= this.images.length ? 0 :
      this.cursor + 1, 'left');
  }

  prev(event?) {
    if (event) event.preventDefault();

    this.move(this.cursor - 1 < 0 ?
      this.images.length - 1 :
      this.cursor - 1, 'right')
  }

  jump(index: number) {
    if (index > this.cursor) {
      this.move(index, 'left');
    } else {
      this.move(index, 'right');
    }
  }

  movingDone(event) {
    switch (event.toState) {
      case 'nextOut':
      case 'prevOut':
        this.moving = false;
        this.images[this.cursor].state = 'active';
        this.images[this.prevCursor].state = 'inactive';
    }
  }

}