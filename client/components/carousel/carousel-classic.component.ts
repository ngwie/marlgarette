import {
  Component,
  Input,
  OnInit,
  OnDestroy } from '@angular/core';

interface CarouselItem {
  url: string;
  className: string;
}

// item prev right
// item active
// item active right

// item next left
// item active
// item active left

@Component({
  selector: 'carousel',
  templateUrl: 'carousel-classic.component.html',
  styleUrls: [ 'carousel-classic.component.css' ]
})
export class CarouselComponent implements OnInit, OnDestroy {
  cursor: number = 0;
  private moving: boolean;
  private interval: NodeJS.Timer;

  @Input()
  images: CarouselItem[];

  ngOnInit() {
    this.images[this.cursor].className = 'active';
    this.setInterval();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  move(index: number, direction: string) {
    if (this.moving) return;
    if (index === this.cursor ||
      index >= this.images.length ||
      index < 0) return;

    this.moving = true;

    if (direction === 'right') {
      this.images[index].className = 'prev';
    } else {
      this.images[index].className = 'next';
    }

    setTimeout(() => {

      if (direction === 'right') {
        this.images[this.cursor].className = 'active right';
        this.images[index].className = 'prev right';
      } else {
        this.images[this.cursor].className = 'active left';
        this.images[index].className = 'next left';
      }

      setTimeout(() => {
        this.moving = false;
        this.images[index].className = 'active';
        this.images[this.cursor].className = '';

        this.cursor = index;
      }, 610);
    }, 10);
  }

  next(event?) {
    if (event) event.preventDefault();

    this.setInterval();
    this.move(this.cursor + 1 >= this.images.length ? 0 :
      this.cursor + 1, 'left');
  }

  prev(event?) {
    if (event) event.preventDefault();

    this.setInterval();
    this.move(this.cursor - 1 < 0 ?
      this.images.length - 1 :
      this.cursor - 1, 'right')
  }

  jump(index: number) {
    if (index > this.cursor) {
      this.move(index, 'left');
    } else {
      this.move(index, 'right');
    }
  }

  setInterval() {
    clearInterval(this.interval);
    this.interval = setInterval(() => this.next(), 8000);
  }

}