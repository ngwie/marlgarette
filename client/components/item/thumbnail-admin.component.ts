import { Component, Input } from '@angular/core';

import { Product } from '../../app/products/product';

@Component({
  selector: 'product-thumb-admin',
  templateUrl: 'thumbnail-admin.component.html',
  styleUrls: [ 'thumbnail-admin.component.css' ]
})
export class ProductItemComponent {

  @Input()
  product: Product;

}