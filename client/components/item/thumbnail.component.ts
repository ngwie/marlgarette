import { Component, Input } from '@angular/core';

import { Product } from '../../app/products/product';

@Component({
  selector: 'product-thumb',
  templateUrl: 'thumbnail.component.html',
  styleUrls: [ 'thumbnail.component.css' ]
})
export class ProductItemComponent {

  @Input()
  product: Product;

}