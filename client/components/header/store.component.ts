import { Component } from '@angular/core';

@Component({
  selector: 'header-store',
  templateUrl: 'store.component.html',
  styleUrls: [ 'store.component.css' ]
})
export class StoreHeaderComponent { }