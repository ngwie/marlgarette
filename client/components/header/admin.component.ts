import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Session } from '../../app/login/session';
import { SessionService } from '../../app/login/session.service';

@Component({
  selector: 'header-admin',
  templateUrl: 'admin.component.html',
  styleUrls: [ 'admin.component.css' ]
})
export class AdminHeaderComponent implements OnInit {
  session: Session;

  constructor(
    private router: Router,
    private sessionService: SessionService) { }

  ngOnInit () {
    this.sessionService.getObservable()
      .subscribe(
        session => this.session = session,
        error =>  console.log(error)
      );
  }

  logout(event) {
    event.preventDefault();

    this.sessionService.logout()
      .then((session) => {
        if (!session.login)
          this.router.navigate(['/login']);
      }, error =>  console.log(error));
  }

}
