import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';

import { AppComponent }   from './app-store.component';
import { routing }        from './app-store.routing';

import { StoreHeaderComponent } from '../components/header/store.component';
import { ProductItemComponent } from '../components/item/thumbnail.component';
import { CarouselComponent } from '../components/carousel/carousel-classic.component';
import { FooterComponent } from '../components/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { StoreComponent } from './store/store.component';
import { SizeChartComponent } from './size-chart/size-chart.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

import { ProductService } from './products/product.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    StoreHeaderComponent,
    HomeComponent,
    StoreComponent,
    SizeChartComponent,
    AboutComponent,
    ContactComponent,
    ProductItemComponent,
    CarouselComponent,
    FooterComponent
  ],
  providers: [
    ProductService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
