import { Component } from '@angular/core';

@Component({
  selector: 'body',
  template: `
    <header-admin></header-admin>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent { }