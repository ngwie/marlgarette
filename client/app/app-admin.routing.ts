import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product.component';
import { ProductNewComponent } from './products/product-new.component';
import { UserSettingsComponent } from './users/user-settings.component'

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'users/settings',
    component: UserSettingsComponent
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: 'products/new',
    component: ProductNewComponent
  },
  {
    path: 'products/:id',
    component: ProductComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);