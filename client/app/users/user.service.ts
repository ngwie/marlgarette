import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { User } from './user';

@Injectable()
export class UserService {

  private userUrl = '/users';
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })
  private requestOption = new RequestOptions({
    headers: this.headers
  });

  constructor(private http: Http) { }

  getUser(userid: string): Promise<User> {
    let url = `${this.userUrl}/${userid}`;
    let requestOption = new RequestOptions({
      body: '',
      headers: this.headers
    });

    return this.http.get(url, requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  updateUserInfo(userid: string, email: string, name: string): Promise<User> {
    let url = `${this.userUrl}/${userid}`;
    let body = JSON.stringify({ email, name });

    return this.http.put(url, body, this.requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  updateUserPassword(userid: string, currentPassword: string, newPassword: string): Promise<User> {
    let url = `${this.userUrl}/${userid}/password`;
    let body = JSON.stringify({ currentPassword, newPassword });

    return this.http.put(url, body, this.requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }

    return res.json() || { };
  }

  private handleError (error: any) {
    let errMsg = error.message || 'Server error';

    console.error(errMsg);
    return Promise.reject(errMsg);
  }

}
