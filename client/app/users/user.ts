
export class User {
  _id: string;
  name: string;
  email: string;
  authority: string;
}