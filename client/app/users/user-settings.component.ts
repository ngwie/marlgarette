import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from './user';
import { Session } from '../login/session';
import { UserService } from './user.service';
import { SessionService } from '../login/session.service';

@Component({
  selector: 'admin-users-settings',
  templateUrl: 'user-settings.component.html',
  styleUrls: [ 'user-settings.component.css' ],
})
export class UserSettingsComponent implements OnInit {
  user: User;
  session: Session;

  currentPassword: string;
  newPassword: string;
  newPasswordConfirm: string;

  constructor(
    private router: Router,
    private userService: UserService,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.sessionService.get()
      .then((session: Session) => {
        if (!session.login)
          return this.router.navigate(['Login']);

        this.session = session;
        this.userService.getUser(session.userid)
          .then((user: User) => {
            this.user = user;
          });
      });
  }

  saveUserInfo() {
    this.userService
      .updateUserInfo(this.session.userid, this.user.email, this.user.name)
      .then((user: User) => {
        this.user = user;
      });
  }

  saveUserPassword() {
    this.userService
      .updateUserPassword(this.session.userid, this.currentPassword, this.newPassword)
      .then((user: User) => {
        this.user = user;

        this.currentPassword = '';
        this.newPassword = '';
        this.newPasswordConfirm = '';
      });
  }
}