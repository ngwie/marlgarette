import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';

import { AppComponent }   from './app-admin.component';
import { routing }        from './app-admin.routing';

import { AdminHeaderComponent } from '../components/header/admin.component';
import { ProductItemComponent } from '../components/item/thumbnail-admin.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product.component';
import { ProductNewComponent } from './products/product-new.component';
import { UserSettingsComponent } from './users/user-settings.component';

import { DROPDOWN_DIRECTIVES } from '../directives/dropdown';

import { ProductService } from './products/product.service';
import { SessionService } from './login/session.service';
import { UserService } from './users/user.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    AdminHeaderComponent,
    LoginComponent,
    DashboardComponent,
    ProductsComponent,
    ProductComponent,
    ProductNewComponent,
    ProductItemComponent,
    UserSettingsComponent,
    DROPDOWN_DIRECTIVES
  ],
  providers: [
    UserService,
    ProductService,
    SessionService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
