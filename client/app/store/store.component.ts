import { Component, OnInit } from '@angular/core';

import { Product, ProductsRes } from '../products/product';
import { ProductService } from '../products/product.service';

@Component({
  selector: 'store',
  templateUrl: 'store.component.html',
  styleUrls: [ 'store.component.css' ]
})
export class StoreComponent implements OnInit {

  products: Product[];
  page: number;
  pageCount: number;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getAll()
      .then(this.assignProducts, error => console.log(error));
  }

  onNext(event: Event) {
    event.preventDefault();

    this.productService.getAll({page: this.page + 1})
      .then(this.assignProducts, error => console.log(error));
  }

  onPrev(event: Event) {
    event.preventDefault();

    this.productService.getAll({page: this.page - 1})
      .then(this.assignProducts, error => console.log(error));
  }

  private assignProducts = (productsRes: ProductsRes) => {
    this.products = productsRes.products;
    this.page = productsRes.page;
    this.pageCount = productsRes.pageCount;
  }
}