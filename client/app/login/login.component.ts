import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Session } from './session';
import { SessionService } from './session.service';

@Component({
  selector: 'admin-login',
  templateUrl: 'login.component.html',
  styleUrls:  ['login.component.css'],
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(
    private router: Router,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.sessionService.get()
      .then(this.onSession);
  }

  login() {
    this.sessionService.login(this.username, this.password)
      .then(this.onSession);
  }

  onSession = (session: Session) => {
    // on session valid
    if (session.login) {
      this.router.navigate(['/dashboard']);
    }
    // on login failed
    else if (!session.login && session.message) {
      // show login failed
    }
  }

}