
export interface Session {
  userid: string;
  username: string;
  login: boolean;
  message: string;
}