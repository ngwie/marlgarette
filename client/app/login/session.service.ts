import { Injectable }     from '@angular/core';
import { Subject }        from 'rxjs/Subject';
import { Observable }     from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Session } from './session';


@Injectable()
export class SessionService {

  private loginUrl = 'login';
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })
  private requestOption = new RequestOptions({
    headers: this.headers
  });
  private session = new Subject<Session>();

  constructor(private http: Http) { }

  get(): Promise<Session> {
    let requestOption = new RequestOptions({
      body: '',
      headers: this.headers
    });

    return this.http.get(this.loginUrl, requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getObservable(): Observable<Session> {
    let requestOption = new RequestOptions({
      body: '',
      headers: this.headers
    });

    this.http.get(this.loginUrl, requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

    return this.session;
  }

  login(username: string, password: string): Promise<Session> {
    let body = JSON.stringify({username, password});

    return this.http.post(this.loginUrl, body, this.requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  logout(): Promise<Session> {
    let requestOption = new RequestOptions({
      body: '',
      headers: this.headers
    });

    return this.http.delete(this.loginUrl, requestOption)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData = (res: Response) => {
    let session: Session = res.json() || { login: false };

    this.session.next(session)
    return session;
  }

  private handleError = (error: any) => {
    let errMsg = error.message || 'Server error';

    console.error(errMsg);
    return Promise.reject(errMsg);
  }

}
