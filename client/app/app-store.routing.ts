import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { StoreComponent } from './store/store.component';
import { SizeChartComponent } from './size-chart/size-chart.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'store',
    component: StoreComponent
  },
  {
    path: 'size-chart',
    component: SizeChartComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);