export interface Price {
  currency: string;
  value: number;
}

export interface Product {
  _id: string;
  name: string;
  brand: string;
  prices: Price[];
  flags: string[];
  pictures: string[];
  displayed: boolean;
  description: string;
  createdAt: string;
  updatedAt: string;
}

export interface ProductRes {
  product: Product;
}

export interface ProductsRes {
  products: Product[];
  page: number;
  pageSize: number;
  pageCount: number;
  totalCount: number;
}