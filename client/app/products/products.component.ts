import { Component, OnInit } from '@angular/core';

import { Product, ProductsRes } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'admin-products',
  templateUrl: 'products.component.html',
  styleUrls: [ 'products.component.css' ]
})
export class ProductsComponent implements OnInit {

  products: Product[];
  page: number;
  pageCount: number;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getAll()
      .then(this.assignProducts, error => console.log(error));
  }

  onNext(event: Event) {
    event.preventDefault();

    this.productService.getAll({page: this.page + 1})
      .then(this.assignProducts, error => console.log(error));
  }

  onPrev(event: Event) {
    event.preventDefault();

    this.productService.getAll({page: this.page - 1})
      .then(this.assignProducts, error => console.log(error));
  }

  private assignProducts = (productsRes: ProductsRes) => {
    this.products = productsRes.products;
    this.page = productsRes.page;
    this.pageCount = productsRes.pageCount;
  }

}