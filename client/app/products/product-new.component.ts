import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'admin-product-new',
  templateUrl: 'product-new.component.html',
  styleUrls: [ 'product-new.component.css' ]
})
export class ProductNewComponent {
  product: Product = {
    _id: null,
    name: null,
    brand: null,
    createdAt: null,
    updatedAt: null,
    displayed: false,
    description: null,
    pictures: [ 'V21DBLK.JPG' ],
    prices: [{
      currency: 'IDR',
      value: null
    }, {
      currency: 'USD',
      value: null
    }],
    flags: [ 'new' ]
  };
  flags: string = 'new';
  pictureFile: File;

  constructor(
    private router: Router,
    private productService: ProductService) { }

  createProduct() {
    this.product.flags = this.flags.split(/\s*,\s*/);
    this.productService.create(this.product)
      .then((productRes) => {
        this.product = productRes.product;

        if (this.pictureFile) {
          this.uploadPicture();
        } else {
          this.router.navigate([ '/products', this.product._id ]);
        }
      }, error => console.error(error));
  }

  uploadPicture() {
    this.productService.uploadPicture(this.product._id, this.pictureFile)
      .then((productRes) => {
        this.router.navigate([ '/products', this.product._id ])
      }, error => console.error(error));
  }

  onChange(event) {
    this.pictureFile = event.srcElement.files[0];
  }
}