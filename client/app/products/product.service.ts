import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { Product, ProductRes, ProductsRes } from './product';

@Injectable()
export class ProductService {

  private productUrl = '/products';
  private requestOptions = new RequestOptions({
    headers: new Headers({
      'Accept': 'application/json'
    })
  });

  constructor(private http: Http) { }

  getAll(query?: any): Promise<ProductsRes> {
    let requestOptions = this.requestOptions.merge({ body: '' });

    if (query) {
      let searchParams = new URLSearchParams();
      for (let key in query) {
        searchParams.set(key, '' + query[key]);
      }
      requestOptions.search = searchParams;
    }

    return this.http.get(this.productUrl, requestOptions)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  get(id: string): Promise<ProductRes> {
    let url = `${this.productUrl}/${id}`;
    let requestOptions = this.requestOptions.merge({ body: '' });

    return this.http.get(url, requestOptions)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  create(product: Product): Promise<ProductRes> {

    return this.http.post(this.productUrl, product, this.requestOptions)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  save(product: Product): Promise<ProductRes> {
    let url = `${this.productUrl}/${product._id}`;

    return this.http.put(url, product, this.requestOptions)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  uploadPicture(id: string, picture: File): Promise<ProductRes> {
    let url = `${this.productUrl}/${id}/pictures`;
    let body = new FormData();
    body.append('picture', picture);

    return this.http.put(url, body, this.requestOptions)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }

    return res.json() || { };
  }

  private handleError(error: any) {
    let errMsg = error.message || 'Server error';

    console.error(errMsg);
    return Promise.reject(errMsg);
  }

}
