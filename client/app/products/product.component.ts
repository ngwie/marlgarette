import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Product, ProductRes } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'admin-product',
  templateUrl: 'product.component.html',
  styleUrls: [ 'product.component.css' ]
})
export class ProductComponent implements OnInit {
  product: Product;
  flags: string;
  pictureFile: File;
  @ViewChild('pictureInput') pictureInput:ElementRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.productService.get(params['id'])
        .then(this.assignProduct, this.handleError);
    });
  }

  saveProduct() {
    this.product.flags = this.flags.split(/\s*,\s*/);
    this.productService.save(this.product)
      .then((productRes) => {
        if (this.pictureFile) this.uploadPicture();
        return productRes;
      })
      .then(this.assignProduct, this.handleError);
  }

  uploadPicture() {
    if (!this.pictureFile) return;

    this.productService.uploadPicture(this.product._id, this.pictureFile)
      .then((productRes) => {
        this.pictureFile = null;
        return productRes;
      })
      .then(this.assignProduct, error => console.error(error));
  }

  onAdd() {
    this.pictureInput.nativeElement.click();
  }

  onReset() {
    this.pictureInput.nativeElement.value = '';
    this.pictureFile = null;
  }

  onChange(event) {
    this.pictureFile = event.srcElement.files[0];
  }

  private assignProduct = (productRes: ProductRes) => {
    this.product = productRes.product;
    this.flags = this.product.flags.reduce(function (prevVal, currVal, index) {
      if (index) prevVal += ', ';
      return prevVal += currVal
    }, '');
  }

  private handleError = (error: any) => {
    console.error(error);
  }
}