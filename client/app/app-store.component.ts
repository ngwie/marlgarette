import { Component } from '@angular/core';

@Component({
  selector: 'body',
  template: `
    <header-store></header-store>
    <router-outlet></router-outlet>
    <footer-store></footer-store>
  `
})
export class AppComponent { }