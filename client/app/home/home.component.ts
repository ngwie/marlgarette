import { Component, OnInit } from '@angular/core';

import { Product, ProductsRes } from '../products/product';
import { ProductService } from '../products/product.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: [ 'home.component.css' ],
})
export class HomeComponent implements OnInit {
  carouselImages = [
    { url: 'desktop-w-bts-f16-v3.jpeg' },
    { url: 'hero-desktop-MMTE-02-f16.jpeg' }
  ];
  newProducts: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getAll({ flag: 'new' })
      .then((productsRes) => {
        this.newProducts = productsRes.products;
      }, error => console.log(error));
  }

}