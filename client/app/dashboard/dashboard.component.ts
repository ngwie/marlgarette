import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';

import { Session } from '../login/session';
import { SessionService } from '../login/session.service';

@Component({
  selector: 'admin-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls:  ['dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.sessionService.get()
      .then((session) => {
        if (!session.login) {
          this.router.navigate(['/login']);
          return;
        }
      }, error => console.log(error));
  }

}