import * as chai from 'chai'
import * as sinon from 'sinon'
import * as request from 'supertest'
import * as passportStub from 'passport-stub'

import app from '../app'
import User, { IUserDocument } from '../models/user'

const expect = chai.expect
const fixtures: any = require('../test/fixtures/models')

describe('UsersController', function () {

  before(function () {
    passportStub.install(app)
  })

  after(function () {
    passportStub.uninstall()
  })

  describe('#POST registration user data', function () {
    let stub

    beforeEach(function () {
      stub = sinon.stub(User, 'create', function (query, callback) {
        callback(null, new User(fixtures.users[0]))
      })
    })

    afterEach(function () {
      stub.restore()
      passportStub.logout()
    })

    it('should create and return a user', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'root' })
      request(app).post('/users')
        .send(fixtures.users[0])
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(res.body).contain.all.keys(
            '_id',
            'name',
            'email',
            'authority'
          )
          expect(res.body).to.not.contain.keys('password')
        })
        .end(done)
    })

    it('should create and return a user', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' })
      request(app).post('/users')
        .send(fixtures.users[0])
        .expect(403)
        .expect('Content-Type', /json/)
        .end(done)
    })
  })

  describe('#GET user data', function () {
    let stub

    beforeEach(function () {
      stub = sinon.stub(User, 'findById', function (query, callback) {
        callback(null, query === 'user_id' ? new User(fixtures.users[0]): null)
      })
    })

    afterEach(function () {
      stub.restore()
    })

    it('should return a user when user is exist', function (done) {
      request(app).get('/users/user_id')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(res.body).contain.all.keys(
            '_id',
            'name',
            'email',
            'authority'
          )
          expect(res.body).to.not.contain.keys('password')
          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('user_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when user is not exist', function (done) {
      request(app).get('/users/not_user_id')
        .expect(404)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('not_user_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })
  })

  describe('#PUT user info update', function () {
    let user: IUserDocument
    let stubFind: Sinon.SinonStub
    let stubSave: Sinon.SinonStub

    beforeEach(function () {
      user = new User(fixtures.users[0])
      stubSave = sinon.stub(user, 'save', function (callback) {
        callback(null, user)
      })
      stubFind = sinon.stub(User, 'findById', function (query, callback) {
        callback(null, query === 'user_id' ? user : null)
      })
      passportStub.login({ id: user._id.toHexString(), name: 'user_name', authority: 'user' })
    })

    afterEach(function () {
      stubFind.restore()
      stubSave.restore()
      passportStub.logout()
    })

    it('should update and return a user when user is exist', function (done) {
      request(app).put('/users/user_id')
        .send(fixtures.users[0])
        .expect('Content-Type', /json/)
        .expect(200)
        .expect(function (res) {
          expect(res.body).contain.all.keys(
            '_id',
            'name',
            'email',
            'authority'
          )
          expect(res.body).to.not.contain.keys('password')
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('user_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubSave.calledOnce).to.be.true
          expect(stubSave.args[0][0]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when user is not exist', function (done) {
      request(app).put('/users/not_user_id')
        .send(fixtures.users[0])
        .expect('Content-Type', /json/)
        .expect(404)
        .expect(function (res) {
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('not_user_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubSave.called).to.be.false
        })
        .end(done)
    })

    it('should return fobidden when user session is not right authority', function (done) {
      passportStub.login({ id: 'not_login_user_id', name: 'user_name', authority: 'user' })
      request(app).put('/users/user_id')
        .send(fixtures.users[0])
        .expect('Content-Type', /json/)
        .expect(403)
        .end(done)
    })
  })

  describe('#PUT user password update', function () {
    let user: IUserDocument
    let stubFind: Sinon.SinonStub
    let stubSave: Sinon.SinonStub
    let stubValidate: Sinon.SinonStub

    beforeEach(function () {
      user = new User(fixtures.users[0])
      stubSave = sinon.stub(user, 'save', function (callback) {
        callback(null, user)
      })
      stubValidate = sinon.stub(user, 'validatePassword', function (password, callback) {
        callback(null, password === 'old_pass')
      })
      stubFind = sinon.stub(User, 'findById', function (query, callback) {
        callback(null, query === 'user_id' ? user : null)
      })
      passportStub.login({ id: user._id.toHexString(), name: 'user_name', authority: 'user' })
    })

    afterEach(function () {
      stubFind.restore()
      stubSave.restore()
      stubValidate.restore()
      passportStub.logout()
    })

    it('should update password and return a user', function (done) {
      request(app).put('/users/user_id/password')
        .send({currentPassword: 'old_pass', newPassword: 'new_pass'})
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(res.body).contain.all.keys(
            '_id',
            'name',
            'email',
            'authority'
          )
          expect(res.body).to.not.contain.keys('password')
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('user_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubValidate.calledOnce).to.be.true
          expect(stubValidate.args[0][0]).to.equal('old_pass')
          expect(stubValidate.args[0][1]).to.be.a('function')
          expect(stubSave.calledOnce).to.be.true
          expect(stubSave.args[0][0]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when user is not exist', function (done) {
      request(app).put('/users/not_user_id/password')
        .send({currentPassword: 'old_pass', newPassword: 'new_pass'})
        .expect(404)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('not_user_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubValidate.called).to.be.false
          expect(stubSave.called).to.be.false
        })
        .end(done)
    })

    it('should return unauthorized when old password invalid', function (done) {
      request(app).put('/users/user_id/password')
        .send({currentPassword: 'not_old_pass', newPassword: 'new_pass'})
        .expect(401)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('user_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubValidate.calledOnce).to.be.true
          expect(stubValidate.args[0][0]).to.equal('not_old_pass')
          expect(stubValidate.args[0][1]).to.be.a('function')
          expect(stubSave.called).to.be.false
        })
        .end(done)
    })

    it('should return forbidden when user session is not right authority', function (done) {
      passportStub.login({ id: 'not_login_user_id', name: 'user_name', authority: 'user' })
      request(app).put('/users/user_id/password')
        .send({currentPassword: 'not_old_pass', newPassword: 'new_pass'})
        .expect(403)
        .expect('Content-Type', /json/)
        .end(done)
    })
  })

})