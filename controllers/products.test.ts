import * as _ from 'lodash'
import * as chai from 'chai'
import * as sinon from 'sinon'
import * as request from 'supertest'
import * as passportStub from 'passport-stub'

import app from '../app'
import Product from '../models/product'
import * as db from '../test/helpers/database'

const expect = chai.expect
const fixtures: any = require('../test/fixtures/models')

describe('Products Controller', function () {

  before(function () {
    passportStub.install(app)
  })

  after(function () {
    passportStub.uninstall()
  })

  describe('#GET products', function () {
    let stubCount: Sinon.SinonStub
    let stubExec: Sinon.SinonStub
    let stubFind: Sinon.SinonStub

    beforeEach(function () {
      let query = Product.find({})

      stubCount = sinon.stub(Product, 'count', function (query, callback) {
        callback(null, 2)
      })

      stubExec = sinon.stub(query, 'exec', function (callback) {
        callback(null, _.map(fixtures.products, (value) => new Product(value)))
      })

      stubFind = sinon.stub(Product, 'find').returns(query)
    })

    afterEach(function () {
      stubCount.restore()
      stubExec.restore()
      stubFind.restore()
    })

    it('should get all product', function (done) {
      request(app).get('/products')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function(res) {
          expect(res.body).contain.all.keys(
            'products',
            'page',
            'pageCount',
            'pageSize',
            'totalCount'
          )
          expect(res.body.products).to.have.lengthOf(2)
        })
        .end(done)
    })
  })

  describe('#POST a new product', function () {
    let stubSave: Sinon.SinonStub
    let stubConstructor: Sinon.SinonStub

    beforeEach(function () {
      let product = new Product();
      stubSave = sinon.stub(product, 'save', function (callback) {
        callback(null, new Product(fixtures.products[0]))
      })
      stubConstructor = sinon.stub(require('../models/product'), 'default').returns(product)
    })

    afterEach(function () {
      stubSave.restore()
      stubConstructor.restore()
      passportStub.logout()
    })

    it('should return forbidden when user is not an admin', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' })
      request(app).post('/products')
        .send(fixtures.products[0])
        .expect(403)
        .expect('Content-Type', /json/)
        .expect(function(res) {
          expect(res.body).contain.all.keys('message')
        })
        .end(done)
    })


    it('should create new product', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' })
      request(app).post('/products')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(200)
        .expect(function (res) {
          expect(res.body).contain.all.keys('product')
          expect(stubConstructor.calledWithNew()).to.be.true
          expect(stubSave.calledOnce).to.be.true
        })
        .end(done)
    })
  })

  describe('#GET a product', function () {
    let stub: Sinon.SinonStub

    beforeEach(function () {
      stub = sinon.stub(Product, 'findById', function (productId, callback) {
        callback(null, productId === 'my_product_id' ? new Product(fixtures.products[0]) : null)
      })
    })

    afterEach(function () {
      stub.restore()
    })

    it('should return product when product is exist', function (done) {
      request(app).get('/products/my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(200)
        .expect(function (res) {
          expect(res.body).contain.all.keys('product')

          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('my_product_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when product is not exist', function (done) {
      request(app).get('/products/not_my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(404)
        .expect(function (res) {
          expect(res.body).contain.all.keys('name', 'message')

          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('not_my_product_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })
  })

  describe('#PUT a product update', function () {
    let stubFind: Sinon.SinonStub
    let stubSave: Sinon.SinonStub

    beforeEach(function () {
      let product = new Product(fixtures.products[0])
      stubSave = sinon.stub(product, 'save', function (callback) {
        callback(null, product)
      })
      stubFind = sinon.stub(Product, 'findById', function (productId, callback) {
        callback(null, productId === 'my_product_id' ? product : null)
      })
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' })
    })

    afterEach(function () {
      stubFind.restore()
      stubSave.restore()
      passportStub.logout()
    })

    it('should update and return product when product is exist', function (done) {
      request(app).put('/products/my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(200)
        .expect(function (res) {
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('my_product_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubSave.calledOnce).to.be.true
          expect(stubSave.args[0][0]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when product is not exist', function (done) {
      request(app).put('/products/not_my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(404)
        .expect(function (res) {
          expect(stubFind.calledOnce).to.be.true
          expect(stubFind.args[0][0]).to.equal('not_my_product_id')
          expect(stubFind.args[0][1]).to.be.a('function')
          expect(stubSave.called).to.be.false
        })
        .end(done)
    })

    it('should return forbidden when user is not an admin', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' })
      request(app).put('/products/not_my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(403)
        .expect(function(res) {
          expect(res.body).contain.all.keys('message')
        })
        .end(done)
    })
  })

  describe('#DELETE a product', function () {
    let stub: Sinon.SinonStub

    beforeEach(function () {
      stub = sinon.stub(Product, 'findByIdAndRemove', function (productId, callback) {
        callback(null, productId === 'my_product_id' ? new Product(fixtures.products[0]) : null)
      })
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' })
    })

    afterEach(function () {
      stub.restore()
      passportStub.logout()
    })

    it('should delete and return product when product is exist', function (done) {
      request(app).delete('/products/my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(200)
        .expect(function (res) {
          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('my_product_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })

    it('should return not found when product is not exist', function (done) {
      request(app).delete('/products/not_my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(404)
        .expect(function (res) {
          expect(stub.calledOnce).to.be.true
          expect(stub.args[0][0]).to.equal('not_my_product_id')
          expect(stub.args[0][1]).to.be.a('function')
        })
        .end(done)
    })

    it('should return forbidden when user is not an admin', function (done) {
      passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' })
      request(app).delete('/products/my_product_id')
        .send(fixtures.products[0])
        .expect('Content-Type', /json/)
        .expect(403)
        .expect(function(res) {
          expect(res.body).contain.all.keys('message')
        })
        .end(done)
    })
  })

  describe('#POST a product picture', function () {})

})