
import * as express from 'express';

let router = express.Router();

/* GET home page. */
router.get(/^\/(?!admin).*/, function(req, res, next) {
  if (!req.accepts('text/html')) return next();

  res.render('index');
});

/* GET admin home page. */
router.get(/^\/admin.*/, function(req, res, next) {
  if (!req.accepts('text/html')) return next();

  res.render('admin');
});

export default router;
