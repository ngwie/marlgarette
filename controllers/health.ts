import * as express from 'express';

let router = express.Router();

router.get('/', function(req, res, next) {
  res.status(200).end();
});

export default router