import * as _ from 'lodash';
import * as express from 'express';
import { Request, Response, NextFunction } from 'express';

import { asRoot, asLoginUser } from '../lib/middleware/auth';
import { NotFoundError, UnauthorizedError } from '../lib/error';
import User, { IUserDocument } from '../models/user';

let router = express.Router();

function createUser(req: Request, res: Response, next: NextFunction) {
  User.create(req.body, function (err, user) {
    if (err) return next(err);

    req.userDoc = user;
    next();
  });
}

function findUserById(req: Request, res: Response, next: NextFunction) {
  User.findById(req.params.userid, function (err, user) {
    if (err) return next(err);
    if (!user) return next(new NotFoundError('User Not Found'));

    req.userDoc = user;
    next();
  });
}

function returnUser(req: Request, res: Response, next: NextFunction) {
  let user: IUserDocument = req.userDoc;

  res.json({
    _id: user._id,
    name: user.name,
    email: user.email,
    authority: user.authority
  });
}

function updateUser(req: Request, res: Response, next: NextFunction) {
  let user: IUserDocument = req.userDoc;

  _.assign(user, _.pick(req.body, ['name', 'email']));
  next();
}

function saveUser(req: Request, res: Response, next: NextFunction) {
  let user: IUserDocument = req.userDoc;

  user.save(function (err, user: IUserDocument) {
    if (err) return next(err);
    next()
  });
}

function validateUserPass(req: Request, res: Response, next: NextFunction) {
  let user: IUserDocument = req.userDoc;

  user.validatePassword(req.body.currentPassword, function (err, valid) {
    if (err) return next(err);
    if (!valid) return next(new UnauthorizedError('Authentication Failed'));

    user.password = req.body.newPassword;
    next();
  });
}

/* POST registration user data. */
router.post('/', asRoot, createUser, returnUser);

/* GET user data. */
router.get('/:userid', findUserById, returnUser);

/* PUT user info update. */
router.put('/:userid', findUserById, asLoginUser, updateUser, saveUser, returnUser);

/* PUT user password update. */
router.put('/:userid/password', findUserById, asLoginUser, validateUserPass, saveUser, returnUser);

export default router;
