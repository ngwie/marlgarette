import * as fs from 'fs';
import * as _ from 'lodash';
import * as path from 'path';
import * as async from 'async';
import * as express from 'express';
import { Request, Response, NextFunction } from 'express';

import User from '../models/user';
import { NotFoundError } from '../lib/error';
import { asAdmin } from '../lib/middleware/auth';
import { productQuery } from '../lib/middleware/query';
import { productPicture } from '../lib/middleware/upload';
import Product, { IProductDocument } from '../models/product';

let router = express.Router();

//
function getProducts(req: Request, res: Response, next: NextFunction) {
  async.parallel<any>([
    function (callback) {
      Product.count(req.query.productQuery, callback);
    },
    function (callback) {
      Product.find(req.query.productQuery)
        .limit(req.query.limit)
        .skip(req.query.skip)
        .exec(callback);
    }
  ], function (err, result) {
    if (err) return next(err);
    if (_.isEmpty(result[1])) return next(new NotFoundError('Products Not Found'));

    res.json({
      products: result[1],
      page: req.query.page,
      pageSize: req.query.pagesize,
      pageCount: Math.ceil(result[0] / req.query.limit),
      totalCount: result[0]
    });
  });
}

//
function getProduct(req: Request, res: Response, next: NextFunction) {
  Product.findById(req.params.id, function (err, product) {
    if (err) return next(err);
    if (_.isEmpty(product)) return next(new NotFoundError('Products Not Found'));

    req.productDoc = product;
    next();
  });
}

//
function createProduct(req: Request, res: Response, next: NextFunction) {
  req.productDoc = new Product();
  next();
}

//
function assignProduct(req: Request, res: Response, next: NextFunction) {
  let product: IProductDocument = req.productDoc;

  _.assign(product, _.pick(req.body, [
    'name',
    'flags',
    'brand',
    'prices',
    'displayed',
    'description'
  ]))

  next();
}

//
function saveProduct(req: Request, res: Response, next: NextFunction) {
  let product: IProductDocument = req.productDoc;

  product.save(next);
}

//
function findAndDeleteProduct(req: Request, res: Response, next: NextFunction) {
  Product.findByIdAndRemove(req.params.id, function(err, product) {
    if (err) return next(err);
    if (_.isEmpty(product)) return next(new NotFoundError('Products Not Found'));

    req.productDoc = product;
    next();
  });
}

//
function updateProductPicture(req: Request, res: Response, next: NextFunction) {
  let product: IProductDocument = req.productDoc;
  let oldPicture = product.pictures;

  product.pictures = [req.file.filename];

  if (_.isEmpty(oldPicture)) return next();

  const config = req.app.get('config');
  const filename = path.join(config.staticDir, config.productImageDir, oldPicture[0]);

  fs.access(filename, function(err) {
    if (err) return next();
    fs.unlink(filename, next);
  });
}

//
function renderProduct(req: Request, res: Response, next: NextFunction) {
  let product: IProductDocument = req.productDoc;
  if (_.isEmpty(product)) return next(new NotFoundError('Product Not Found'));

  res.json({ product });
}


/* GET products. */
router.get('/', productQuery, getProducts);

/* POST a new product. */
router.post('/', asAdmin, createProduct, assignProduct, saveProduct, renderProduct);

/* GET a product. */
router.get('/:id', getProduct, renderProduct);

/* PUT a product update. */
router.put('/:id', asAdmin, getProduct, assignProduct, saveProduct, renderProduct);

/* DELETE a product. */
router.delete('/:id', asAdmin, findAndDeleteProduct, renderProduct);

/* PUT a product picture. */
router.put('/:id/pictures', asAdmin, getProduct, productPicture.single('picture'),
  updateProductPicture, saveProduct, renderProduct);

export default router;
