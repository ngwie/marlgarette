import * as request from 'supertest'
import app from '../app'


describe('Health Controller', function () {

  describe('#GET health status', function () {

    it('should return status code 200', function (done) {
      request(app).get('/health')
        .expect(200)
        .end(done)
    })

  })

})