import { authenticate } from '../lib/passport';
import * as express from 'express';
import { Request, Response, NextFunction } from 'express';

import User from '../models/user';

let router = express.Router();

function getUserSession(req: Request, res: Response, next: NextFunction) {
  if (!req.isAuthenticated()) return res.json({ login: false });

  res.json({
    login: true,
    userid: req.user.id,
    username: req.user.name
  });
}

function deleteUserSession(req: Request, res: Response, next: NextFunction) {
  req.logout();
  next();
}

/* GET user session. */
router.get('/', getUserSession);

/* POST user login data. */
router.post('/', authenticate, getUserSession);

/* DELETE user session. */
router.delete('/', deleteUserSession, getUserSession);

export default router;
