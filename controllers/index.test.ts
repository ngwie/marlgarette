import * as request from 'supertest'
import app from '../app'

describe('Index Controller', function () {

  describe('#GET home page', function () {

    it('should render store home page when request accept text/html', function (done) {
      request(app).get('/')
        .expect(200)
        .expect('Content-Type', /html/)
        .expect(/<base href="\/">/)
        .end(done)
    })

    it('should not render store home page when request does not accept text/html', function (done) {
      request(app).get('/')
        .set('Accept', 'application/json')
        .expect(404)
        .expect('Content-Type', /json/)
        .end(done)
    })
  })

  describe('#GET admin home page', function () {

    it('should render admin home page when request accept text/html', function (done) {
      request(app).get('/admin')
        .expect(200)
        .expect('Content-Type', /html/)
        .expect(/<base href="\/admin">/)
        .end(done)
    })

    it('should not render admin home page when request does not accept text/html', function (done) {
      request(app).get('/admin')
        .set('Accept', 'application/json')
        .expect(404)
        .expect('Content-Type', /json/)
        .end(done)
    })
  })

})