/**
 * Create root user
 */

var config = require('config');
var mongoose = require('mongoose');

var database = require('../build/lib/database').default;
var User = require('../build/models/user').default;

var dbconf = config.get('database');

database(dbconf, function () {
  User.create({
    name: 'miller.jr',
    email: 'f4rh4n.sen4ng@gmail.com',
    password: 'root',
    authority: 'root'
  }, function (err) {
    if (err) {
      console.err(err);
    } else {
      console.log('user created');
    }

    mongoose.disconnect();
  });
});
