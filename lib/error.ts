
// Bad Request error 400
export class BadRequestError extends Error {
  name = 'BadRequestError';

  constructor(public message: string) {
		super(message);
	}
}

// Unauthorized error 401
export class UnauthorizedError extends Error {
  name = 'UnauthorizedError';

  constructor(public message: string) {
		super(message);
	}
}

// Forbidden error 403
export class ForbiddenError extends Error {
  name = 'ForbiddenError';

  constructor(public message: string) {
		super(message);
	}
}

// Not found error 404
export class NotFoundError extends Error {
  name: string = 'NotFoundError';

  constructor(public message: string) {
		super(message);
	}
}

// Server error 500
export class ServerError extends Error {
  name: string = 'ServerError';

  constructor(public message: string) {
		super(message);
	}
}