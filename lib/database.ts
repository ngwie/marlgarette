
import * as mongoose from 'mongoose';
import logger from './logger';

// set mongoose promise to use native promises
require('mongoose').Promise = global.Promise;

export interface IDatabaseConfig {
  database: string,
  host: string
}

export default function (config: IDatabaseConfig, cb?: () => void) {

  if (mongoose.connection.readyState !== 0) {
    if (cb) cb();
    return;
  }

  const uri = config.host + config.database;

  mongoose.connect(uri);

  mongoose.connection.on('error', function (error) {
    logger.error('Database connection error:', error);
  });

  mongoose.connection.once('open', function () {
    if (cb) cb();
    logger.info('Database connection open');
  });

};

export function closeConnection() {
  mongoose.connection.close()
}