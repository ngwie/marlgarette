import * as bunyan from 'bunyan';
import * as uuid from 'node-uuid';
import * as express from 'express';
import * as _ from 'lodash';

let logger = bunyan.createLogger({
  name: 'marlgarette',
  level: 'debug',
  serializers: {
    req: bunyan.stdSerializers.req,
    res: bunyan.stdSerializers.res,
    err: bunyan.stdSerializers.err
  }
});

function filterReq(req: express.Request): express.Request {
  let obj: any = _.cloneDeep(req);
  obj.headers = _.omit(req.headers, 'cookie');
  return obj;
}

export function requestLogger(req: express.Request, res: express.Response, next: express.NextFunction) {
  let start = Date.now();
  let requestId = req.headers['x-request-id'] || uuid.v4();

  req.logger = logger.child({ 'reqId': requestId });

  res.on('finish', function() {
    req.logger.info({
      duration:  Date.now() - start,
      req: filterReq(req),
      res: res
    }, 'request finish');
  })

  res.on('close', function () {
    req.logger.warn({
      duration:  Date.now() - start,
      req: filterReq(req),
      res: res
    }, 'request socket closed');
  })

  next();
}

export default logger;