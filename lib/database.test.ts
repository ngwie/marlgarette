import * as chai from 'chai'

import * as db from '../test/helpers/database'
import Product from '../models/product'

let expect = chai.expect

describe('DB Connetion', function () {

  before(function (done) {
    db.connect(done);
  })

  describe('#promise', function() {

    it('should using native promise', function() {
      let query = Product.findOne({id: 'test_id'})

      expect(query.exec()).to.be.instanceof(global.Promise)
    })
  })
})
