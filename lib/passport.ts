
import * as passport from 'passport';
import { Strategy } from 'passport-local';
import { UnauthorizedError } from './error';
import { Request, Response, NextFunction } from 'express';

import User from '../models/user';

passport.use(new Strategy(function (username, password, done) {
  User.findOne({ email: username }, function (err, user) {
    if (err) return done(err);
    if (!user) return done(null, false, { message: 'Incorrect username.' });

    user.validatePassword(password, function(err, res) {
      if (err) return done(err);

      if (res) {
        done(null, user);
      } else {
        done(null, false, { message: 'Incorrect password.' });
      }
    });
  });
}));

passport.serializeUser(function(user, done) {
  done(null, {
    id: user.id,
    name: user.name,
    authority: user.authority
  });
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

export function authenticate(req: Request, res: Response, next: NextFunction) {
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err);
    if (!user) return next(new UnauthorizedError('Incorrect username or password'));

    req.logIn(user, next);
  })(req, res, next);
}

export default passport;