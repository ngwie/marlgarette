import * as _ from 'lodash';
import logger, { requestLogger } from '../logger';

export default function <ErrorRequestHandler>(err, req, res, next) {
  logger.warn(err);

  let errorCode;

  if (
    err.name === 'BadRequestError' ||
    _.includes(['ValidationError', 'CastError'], err.name)
  ) {
    errorCode = 400;
  } else if (err.name === 'UnauthorizedError') {
    errorCode = 401;
  } else if (err.name === 'ForbiddenError') {
    errorCode = 403;
  } else if (err.name === 'NotFoundError') {
    errorCode = 404;
  } else {
    errorCode = 500;
  }

  res.status(errorCode);
  res.json({
    name: err.name,
    message: err.message
  });
}