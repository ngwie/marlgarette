import { Request, Response, NextFunction } from 'express';

export function pageQuery(req: Request, res: Response, next: NextFunction) {
  let page = req.query.page = parseInt(req.query.page) || 1;
  let pageSize = req.query.pagesize = parseInt(req.query.pagesize) || 9;

  req.query.limit = pageSize;
  req.query.skip = pageSize * (page - 1);
  next();
}

export function productQuery(req: Request, res: Response, next: NextFunction) {
  let query: any = {};
  let flag: string = req.query.flag;

  if (flag)
    query.flags = flag;

  req.query.productQuery = query;
  pageQuery(req, res, next);
}
