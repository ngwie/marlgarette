import * as _ from 'lodash';
import * as path from 'path';
import * as mime from 'mime';
import * as multer from 'multer';
import * as randomstring from 'randomstring';
import { Request } from 'express';

export const productPicture = multer({

  fileFilter(req, file, cb) {
    let acceptedType = [ 'image/jpeg', 'image/png' ];
    cb(null, _.includes(acceptedType, file.mimetype))
  },

  storage: multer.diskStorage({

    destination(req: Request, file, cb) {
      const config = req.app.get('config');

      cb(null, path.join(config.staticDir, config.productImageDir));
    },

    filename(req, file, cb) {
      let name = _.kebabCase(req.productDoc.name) +
        '-' + randomstring.generate(12) +
        '.' + mime.extension(file.mimetype);
      cb(null, name)
    }
  })

});
