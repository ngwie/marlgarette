import { Request, Response, NextFunction } from 'express';
import { UnauthorizedError, ForbiddenError, ServerError } from '../error';

const UNAUTHORIZED_ERR_MSG = 'Authentication credentials were missing or incorrect.';
const FORBIDDEN_ERR_MSG = 'The request is understood, but it has been refused or access is not allowed.';
const SERVER_ERR_MSG = 'User data should be provided';

export function asAdmin(req: Request, res: Response, next: NextFunction) {
  if (!req.user) {
    return next(new UnauthorizedError(UNAUTHORIZED_ERR_MSG));
  }

  if (req.user.authority !== 'admin') {
    return next(new ForbiddenError(FORBIDDEN_ERR_MSG));
  }

  next();
}

export function asRoot(req: Request, res: Response, next: NextFunction) {
  if (!req.user) {
    return next(new UnauthorizedError(UNAUTHORIZED_ERR_MSG));
  }

  if (req.user.authority !== 'root') {
    return next(new ForbiddenError(FORBIDDEN_ERR_MSG));
  }

  next();
}

export function asLoginUser(req: Request, res: Response, next: NextFunction) {
  if (!req.user) {
    return next(new UnauthorizedError(UNAUTHORIZED_ERR_MSG));
  }

  if (!req.userDoc) {
    return next(new ServerError(SERVER_ERR_MSG));
  }

  if (req.userDoc._id.toHexString() !== req.user.id) {
    return next(new ForbiddenError(FORBIDDEN_ERR_MSG));
  }

  next();
}