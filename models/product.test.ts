
import * as chai from 'chai'

import * as db from '../test/helpers/database'
import Product, { IProductDocument } from './product'

let expect = chai.expect
let fixtures: any = require('../test/fixtures/models')

describe('ProductModel', function () {

  before(function (done) {
    db.connect(done)
  })

  afterEach(function (done) {
    Product.remove({}, done)
  })

  describe('#create', function () {
    let product: IProductDocument

    beforeEach(function () {
      product = new Product(fixtures.products[1])
    })

    it('should assign false in displayed as a defult value when displayed is blank', function () {
      expect(product.displayed).to.be.false
    })

    it('should assign "IDR" in price.currency as a defult value when price.currency is blank', function () {
      expect(product.prices[0].currency).to.be.equal('IDR')
    })
  })

})

describe('ProductDocument', function () {

  before(function (done) {
    db.connect(done)
  })

  describe('#validate', function () {
    let product: IProductDocument

    beforeEach(function () {
      product = new Product(fixtures.products[0])
    })

    it('should not return error when all value is valid', function (done) {
      product.validate(function (error) {
        expect(!error).to.be.ok
        done()
      })
    })

    it('should return error when name is blank', function (done) {
      product.name = ''

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['name'].message).to.equal('name is required')
        done()
      })
    })

    it('should return error when brand is blank', function (done) {
      product.brand = null

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['brand'].message).to.equal('brand is required')
        done()
      })
    })

    it('should return error when displayed is blank', function (done) {
      product.displayed = null

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['displayed'].message).to.equal('displayed is required')
        done()
      })
    })

    it('should return error when prices is blank', function (done) {
      product.prices = null

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['prices'].message).to.equal('prices is required')
        done()
      })
    })

    it('should return error when price.currency is blank', function (done) {
      product.prices[0].currency = null

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['prices.0.currency'].message).to.equal('price currency is required')
        done()
      })
    })

    it('should return error when price.currency value is invalid', function (done) {
      product.prices[0].currency = 'EUR'

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['prices.0.currency'].message).to.equal('currency is not supported')
        done()
      })
    })

    it('should return error when price.value is blank', function (done) {
      product.prices[0].value = null

      product.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['prices.0.value'].message).to.equal('price value is required')
        done()
      })
    })
  })

})