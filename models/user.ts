
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';

export interface IUserDocument extends mongoose.Document {
  name: string;
  email: string;
  password: string;
  authority: string;
  validatePassword(password: string, callback: (error: any, valid: boolean) => void): void;
}

export interface IUserModel extends mongoose.Model<IUserDocument> {
  // findByName(password: string, salt: string): string;
}

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [ true, 'name is required' ]
  },

  email: {
    type: String,
    unique: true,
    required: [ true, 'email is required' ],
    match: [ /\S+@\S+\.\S+/, 'email is invalid' ]
  },

  password: {
    type: String,
    required: [ true, 'password is required' ]
  },

  authority: {
    type: String,
    default: 'customer',
    required: [ true, 'authority is required' ],
    enum: {
      values: [ 'customer', 'admin', 'root' ],
      message: 'invalid authority value'
    }
  }
}, {
  timestamps: true
});

userSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next();

  bcrypt.hash(this.password, 10, (err, hash) => {
    if (err) return next(err);

    this.password = hash;
    next();
  });
});

userSchema.method('validatePassword', function (password: string, next: any) {
  bcrypt.compare(password, this.password, next);
});

const User = <IUserModel>mongoose.model('User', userSchema);

export default User;
