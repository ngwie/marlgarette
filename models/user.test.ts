
import * as chai from 'chai'
import * as async from 'async'
import * as bcrypt from 'bcryptjs'

import * as db from '../test/helpers/database'
import User, { IUserDocument } from './user'

let expect = chai.expect
let fixtures: any = require('../test/fixtures/models')

describe('User Model', function () {
  let user: IUserDocument

  before(function (done) {
    db.connect(done)
  })

  beforeEach(function (done) {
    User.create([fixtures.users[0]], function (err, result) {
      user = result[0]
      done(err)
    })
  })

  afterEach(function (done) {
    User.remove({}, done)
  })

  describe('#create', function () {

    it('should assign user value correctly', function () {
      let userData = fixtures.users[0]

      expect(user.name).to.equal(userData.name)
      expect(user.email).to.equal(userData.email)
      expect(user.authority).to.equal(userData.authority)
    })

    it('should assign "customer" as authority defult value when authority is blank', function () {
      let user = new User(fixtures.users[1])

      expect(user.authority).to.equal('customer')
    })

    it('should return error when email is already exist', function (done) {
      User.create(fixtures.users[0], function (error, result) {
        expect(!error).to.not.be.ok
        expect(error.name).to.equal('MongoError')
        expect(error.code).to.equal(11000)
        done()
      })
    })

    it('should change password with hashed password', function (done) {
      expect(user.password).to.not.equal('test')

      bcrypt.compare('test', user.password, function (err, result) {
        expect(result).to.be.true
        done(err)
      })
    })

  })

  describe('#save', function () {

    it('should change password with hashed password when password update', function (done) {
      user.password = 'testnew'
      user.save(function (err, user: IUserDocument) {
        if (err) return done(err)
        expect(user.password).to.not.equal('testnew')

        bcrypt.compare('testnew', user.password, function (err, result) {
          expect(result).to.be.true
          done(err)
        })
      })
    })

    it('should not change password with hashed password when password not updateed', function (done) {
      user.name = 'Miller Junior'
      user.save(function (err, user: IUserDocument) {
        if (err) return done(err)

        bcrypt.compare('test', user.password, function (err, result) {
          expect(result).to.be.true
          done(err)
        })
      })
    })

  })

})

describe('User Document', function () {

  before(function (done) {
    db.connect(done)
  })

  describe('#validatePassword', function () {
    let user: IUserDocument

    beforeEach(function (done) {
      User.create([fixtures.users[0]], function (err, result) {
        user = result[0]
        done(err)
      })
    })

    afterEach(function (done) {
      User.remove({}, done)
    })

    it('should return true when password valid', function (done) {
      user.validatePassword('test', function (err, result) {
        expect(result).to.be.true
        done(err)
      })
    })

    it('should return false when password invalid', function (done) {
      user.validatePassword('testnew', function (err, result) {
        expect(result).to.be.false
        done(err)
      })
    })

  })

  describe('#validate', function () {
    let user: IUserDocument

    beforeEach(function () {
      user = new User(fixtures.users[0])
    })

    it('should not return error when name, email, password, and authority are not blank', function (done) {
      user.validate(function (error) {
        expect(!error).to.be.ok
        done()
      })
    })

    it('should return error when name is blank', function (done) {
      user.name = null
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['name'].message).to.equal('name is required')
        done()
      })
    })

    it('should return error when email is blank', function (done) {
      user.email = null
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['email'].message).to.equal('email is required')
        done()
      })
    })

    it('should return error when email format is invalid', function (done) {
      user.email = 'not_email_format'
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['email'].message).to.equal('email is invalid')
        done()
      })
    })

    it('should return error when password is blank', function (done) {
      user.password = null
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['password'].message).to.equal('password is required')
        done()
      })
    })

    it('should return error when authority is blank', function (done) {
      user.authority = null
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['authority'].message).to.equal('authority is required')
        done()
      })
    })

    it('should not return error when authority value is "customer", "admin", or "root"', function (done) {
      let validAuthorityValue = [ 'customer', 'admin', 'root' ]

      async.eachSeries(validAuthorityValue, function (item, cb) {
        user.authority = item
        user.validate(function (error) {
          expect(!error).to.be.ok
          cb()
        })
      }, done)
    })

    it('should return error when authority value is other then "customer", "admin", and "root"', function (done) {
      user.authority = 'invalid_authority'
      user.validate(function (error) {
        expect(!error).to.not.be.ok
        expect(error.errors['authority'].message).to.equal('invalid authority value')
        done()
      })
    })

  })

})