
import * as mongoose from 'mongoose';

export interface Price {
  currency: string;
  value: number;
}

export interface IProductDocument extends mongoose.Document {
  name: string;
  brand: string;
  sizes: number[];
  quantity: any;
  prices: Price[];
  flags: string[];
  pictures: string[];
  displayed: Boolean;
  description: string;
}

export interface IProductModel extends mongoose.Model<IProductDocument> {
  // findByName(password: string, salt: string): string;
}

const priceSchema = new mongoose.Schema({
  currency: {
    type: String,
    default: 'IDR',
    required: [ true, 'price currency is required' ],
    enum: {
      values: [ 'IDR', 'USD' ],
      message: 'currency is not supported'
    }
  },

  value: {
    type: Number,
    required: [ true, 'price value is required' ]
  }
});

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [ true, 'name is required' ]
  },

  brand: {
    type: String,
    required: [ true, 'brand is required' ]
  },

  displayed: {
    type: Boolean,
    default: false,
    required: [ true, 'displayed is required' ]
  },

  prices: {
    type: [ priceSchema ],
    required: [ true, 'prices is required' ]
  },

  sizes: Array,
  quantity: Object,
  flags: [ String ],
  pictures: Array,
  description: String,
}, {
  timestamps: true
});

const Product = <IProductModel>mongoose.model('Product', productSchema);

export default Product;