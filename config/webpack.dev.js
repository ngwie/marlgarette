var path = require('path');
var appRoot = require('app-root-path');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',

  output: {
    path: path.join(appRoot.path, 'public', 'javascripts'),
    filename: '[name].js'
  },

  plugins: [
    new ExtractTextPlugin('[name].css')
  ]
});