
module.exports = {
  server: {
    host: process.env.NODE_IP,
    port: process.env.NODE_PORT
  },
  database: {
    host: process.env.MONGODB_URL
  },
  app: {
    staticDir: process.env.OPENSHIFT_DATA_DIR
  }
}