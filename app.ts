import * as path from 'path';
import * as config from 'config';
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as appRoot from 'app-root-path';
import * as favicon from 'serve-favicon';
import * as bodyParser from 'body-parser';
import * as session from 'express-session';
import * as connectMongo from 'connect-mongo';
import * as cookieParser from 'cookie-parser';

import logger, { requestLogger } from './lib/logger';
import passport from './lib/passport';
import database, { IDatabaseConfig } from './lib/database';
import errorHandler from './lib/middleware/error-handler';

import Health from './controllers/health';
import Index from './controllers';
import Users from './controllers/users';
import Login from './controllers/login';
import Products from './controllers/products';

import { NotFoundError } from './lib/error';

let app = express();
let MongoStore = connectMongo(session);

// view engine setup
app.set('views', path.join(appRoot.path, 'views'));
app.set('view engine', 'ejs');
app.set('config', config.get('app'));

// setup database
database(config.get<IDatabaseConfig>('database'));

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: 'random_string_key',
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
app.use(passport.initialize());
app.use(passport.session());
if (app.get('env') === 'development') {
  app.use(require('less-middleware')(path.join(__dirname, 'public')));
}
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(app.get('config').staticDir, 'images')));

// setup logger
logger.level(app.get('env') === 'test' ? 'error' : 'debug');
app.use(requestLogger);

app.use('/health', Health);
app.use('/users',Users);
app.use('/login', Login);
app.use('/products', Products);
app.use('/', Index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(new NotFoundError('Path Not Found'));
});

// error handler
app.use(errorHandler);

export default app;

