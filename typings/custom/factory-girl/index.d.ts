
declare module "factory-girl" {

  namespace factory {
    export function define(name: string, model: any, attributes: any, options?: any): void;

    export function attrs(name: string, callback: (error: any, result: any) => void): void;
    export function attrs(name: string, attributes: any, callback: (error: any, result: any) => void): void;
    export function attrs(name: string, attributes: any, options: any, callback: (error: any, result: any) => void): void;

  	export function build(name: string, callback: (error: any, result: any) => void): void;
    export function build(name: string, attributes: any, callback: (error: any, result: any) => void): void;
    export function build(name: string, attributes: any, options: any, callback: (error: any, result: any) => void): void;

    export function create(name: string, callback: (error: any, result: any) => void): void;
    export function create(name: string, attributes: any, callback: (error: any, result: any) => void): void;
    export function create(name: string, attributes: any, options: any, callback: (error: any, result: any) => void): void;
  }

  export = factory;
}