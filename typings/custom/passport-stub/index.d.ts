
declare module "passport-stub" {
  import * as express from 'express'

  namespace passportStub {
    export function install(app: express.Express): void;
    export function uninstall(): void;
    export function login(user: Object): void;
    export function logout(): void;
  }

  export = passportStub;
}