
# REST API Documentation

## Products

### GET /products

Description:
Get all products

Params:
flag - valid value is `new`
page - accept integer start from 1, default value is 1
pagesize - accept interger start from 1 to 100, default value is 12

Respond:
```
HTTP/1.1 200
Content-Type: application/json

{
  "products": [
    {
      "id": "57665eb7f09494095e8e5f66",
      "name" : "Monument",
      "brand" : "Marlgarette",
      "flags": [ "new" ],
      "displayed" : true,
      "description" : "New margarette product",
      "pictures" : [
        "V21DBLK.JPG"
      ],
      "prices" : [
        {
          "currency": "ird",
          "value" : 110
        }
      ],
      "sizes" : []
    }
  ],
  "page": 5,
  "pageCount": 5,
  "pageSize": 20,
  "totalCount": 100
}
```

### GET /products/:id

Description:
Get one spesific product

Params:
-

Respond:
```
HTTP/1.1 200
Content-Type: application/json

{
  "product": {
    "id": "57665eb7f09494095e8e5f66",
    "name" : "Monument",
    "brand" : "Marlgarette",
    "flags": [ "new" ],
    "displayed" : true,
    "description" : "New margarette product",
    "pictures" : [
      "V21DBLK.JPG"
    ],
    "prices" : [
      {
        "currency": "ird",
        "value" : 110
      }
    ],
    "sizes" : []
  }
}
```

### POST /products

Description:
Create a new product

Params:
```
{
  "name" : "Monument",
  "brand" : "Marlgarette",
  "displayed" : true,
  "flags": [ "new" ],
  "description" : "New margarette product",
  "prices" : [
    {
      "currency": "ird",
      "value" : 110
    }
  ],
  "sizes" : []
}
```

Respond:
```
HTTP/1.1  201
Content-Type: application/json

{
  "product": {
    "id": "57665eb7f09494095e8e5f66",
    "name" : "Monument",
    "brand" : "Marlgarette",
    "flags": [ "new" ],
    "displayed" : true,
    "description" : "New margarette product",
    "pictures" : [
      "V21DBLK.JPG"
    ],
    "prices" : [
      {
        "currency": "ird",
        "value" : 110
      }
    ],
    "sizes" : []
  }
}
```

### PATCH /products/:id

Params:
```
{
  "name" : "Monument",
  "brand" : "Marlgarette",
  "displayed" : true,
  "flags": [ "new" ],
  "description" : "New margarette product",
  "prices" : [
    {
      "currency": "ird",
      "value" : 110
    }
  ],
  "sizes" : []
}
```

Respond:
```
HTTP/1.1  201
Content-Type: application/json

{
  "product": {
    "id": "57665eb7f09494095e8e5f66",
    "name" : "Monument",
    "brand" : "Marlgarette",
    "flags": [ "new" ],
    "displayed" : true,
    "description" : "New margarette product",
    "pictures" : [
      "V21DBLK.JPG"
    ],
    "prices" : [
      {
        "currency": "ird",
        "value" : 110
      }
    ],
    "sizes" : []
  },
  "message": "The item was updated successfully"
}
```

### DELETE /products/:id

Params:
-

Respond:
```
HTTP/1.1  200
Content-Type: application/json

{
  "product": {
    "id": "57665eb7f09494095e8e5f66",
    "name" : "Monument",
    "brand" : "Marlgarette",
    "flags": [ "new" ],
    "displayed" : true,
    "description" : "New margarette product",
    "pictures" : [
      "V21DBLK.JPG"
    ],
    "prices" : [
      {
        "currency": "ird",
        "value" : 110
      }
    ],
    "sizes" : []
  },
  "message": "The item was deleted successfully"
}
```