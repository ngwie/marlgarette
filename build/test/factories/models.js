"use strict";
var factory = require('factory-girl');
var user_1 = require('../../models/user');
factory.define('user', user_1.default, {
    name: 'Miller .Jr',
    email: 'miller.jr@gmail.com',
    password: 'test',
    authority: 'admin'
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = factory;
