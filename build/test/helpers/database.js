"use strict";
var mongoose = require('mongoose');
var config = require('config');
var database_1 = require('../../lib/database');
function connect(next) {
    if (mongoose.connection.readyState !== 0)
        return next();
    var databaseConfig = config.get('database');
    database_1.default(databaseConfig, next);
}
exports.connect = connect;
function getDB() {
    return mongoose.connection.db;
}
exports.getDB = getDB;
function drop(done) {
    mongoose.connection.db.dropDatabase(done);
}
exports.drop = drop;
function fixtures(data, done) {
    /*
    var db = state.db
    if (!db) {
      return done(new Error('Missing database connection.'))
    }
  
    var names = Object.keys(data.collections)
    async.each(name, function(name, cb) {
      db.createCollection(name, function(err, collection) {
        if (err) return cb(err)
        collection.insert(data.collections[name], cb)
      })
    }, done)
    */
}
exports.fixtures = fixtures;
