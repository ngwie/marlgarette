"use strict";
var request = require('supertest');
var app_1 = require('../app');
describe('Health Controller', function () {
    describe('#GET health status', function () {
        it('should return status code 200', function (done) {
            request(app_1.default).get('/health')
                .expect(200)
                .end(done);
        });
    });
});
