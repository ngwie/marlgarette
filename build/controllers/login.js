"use strict";
var passport_1 = require('../lib/passport');
var express = require('express');
var router = express.Router();
function getUserSession(req, res, next) {
    if (!req.isAuthenticated())
        return res.json({ login: false });
    res.json({
        login: true,
        userid: req.user.id,
        username: req.user.name
    });
}
function deleteUserSession(req, res, next) {
    req.logout();
    next();
}
/* GET user session. */
router.get('/', getUserSession);
/* POST user login data. */
router.post('/', passport_1.authenticate, getUserSession);
/* DELETE user session. */
router.delete('/', deleteUserSession, getUserSession);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
