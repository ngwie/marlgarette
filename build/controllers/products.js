"use strict";
var fs = require('fs');
var _ = require('lodash');
var path = require('path');
var async = require('async');
var express = require('express');
var error_1 = require('../lib/error');
var auth_1 = require('../lib/middleware/auth');
var query_1 = require('../lib/middleware/query');
var upload_1 = require('../lib/middleware/upload');
var product_1 = require('../models/product');
var router = express.Router();
//
function getProducts(req, res, next) {
    async.parallel([
        function (callback) {
            product_1.default.count(req.query.productQuery, callback);
        },
        function (callback) {
            product_1.default.find(req.query.productQuery)
                .limit(req.query.limit)
                .skip(req.query.skip)
                .exec(callback);
        }
    ], function (err, result) {
        if (err)
            return next(err);
        if (_.isEmpty(result[1]))
            return next(new error_1.NotFoundError('Products Not Found'));
        res.json({
            products: result[1],
            page: req.query.page,
            pageSize: req.query.pagesize,
            pageCount: Math.ceil(result[0] / req.query.limit),
            totalCount: result[0]
        });
    });
}
//
function getProduct(req, res, next) {
    product_1.default.findById(req.params.id, function (err, product) {
        if (err)
            return next(err);
        if (_.isEmpty(product))
            return next(new error_1.NotFoundError('Products Not Found'));
        req.productDoc = product;
        next();
    });
}
//
function createProduct(req, res, next) {
    req.productDoc = new product_1.default();
    next();
}
//
function assignProduct(req, res, next) {
    var product = req.productDoc;
    _.assign(product, _.pick(req.body, [
        'name',
        'flags',
        'brand',
        'prices',
        'displayed',
        'description'
    ]));
    next();
}
//
function saveProduct(req, res, next) {
    var product = req.productDoc;
    product.save(next);
}
//
function findAndDeleteProduct(req, res, next) {
    product_1.default.findByIdAndRemove(req.params.id, function (err, product) {
        if (err)
            return next(err);
        if (_.isEmpty(product))
            return next(new error_1.NotFoundError('Products Not Found'));
        req.productDoc = product;
        next();
    });
}
//
function updateProductPicture(req, res, next) {
    var product = req.productDoc;
    var oldPicture = product.pictures;
    product.pictures = [req.file.filename];
    if (_.isEmpty(oldPicture))
        return next();
    var config = req.app.get('config');
    var filename = path.join(config.staticDir, config.productImageDir, oldPicture[0]);
    fs.access(filename, function (err) {
        if (err)
            return next();
        fs.unlink(filename, next);
    });
}
//
function renderProduct(req, res, next) {
    var product = req.productDoc;
    if (_.isEmpty(product))
        return next(new error_1.NotFoundError('Product Not Found'));
    res.json({ product: product });
}
/* GET products. */
router.get('/', query_1.productQuery, getProducts);
/* POST a new product. */
router.post('/', auth_1.asAdmin, createProduct, assignProduct, saveProduct, renderProduct);
/* GET a product. */
router.get('/:id', getProduct, renderProduct);
/* PUT a product update. */
router.put('/:id', auth_1.asAdmin, getProduct, assignProduct, saveProduct, renderProduct);
/* DELETE a product. */
router.delete('/:id', auth_1.asAdmin, findAndDeleteProduct, renderProduct);
/* PUT a product picture. */
router.put('/:id/pictures', auth_1.asAdmin, getProduct, upload_1.productPicture.single('picture'), updateProductPicture, saveProduct, renderProduct);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
