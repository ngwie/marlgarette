"use strict";
var _ = require('lodash');
var express = require('express');
var auth_1 = require('../lib/middleware/auth');
var error_1 = require('../lib/error');
var user_1 = require('../models/user');
var router = express.Router();
function createUser(req, res, next) {
    user_1.default.create(req.body, function (err, user) {
        if (err)
            return next(err);
        req.userDoc = user;
        next();
    });
}
function findUserById(req, res, next) {
    user_1.default.findById(req.params.userid, function (err, user) {
        if (err)
            return next(err);
        if (!user)
            return next(new error_1.NotFoundError('User Not Found'));
        req.userDoc = user;
        next();
    });
}
function returnUser(req, res, next) {
    var user = req.userDoc;
    res.json({
        _id: user._id,
        name: user.name,
        email: user.email,
        authority: user.authority
    });
}
function updateUser(req, res, next) {
    var user = req.userDoc;
    _.assign(user, _.pick(req.body, ['name', 'email']));
    next();
}
function saveUser(req, res, next) {
    var user = req.userDoc;
    user.save(function (err, user) {
        if (err)
            return next(err);
        next();
    });
}
function validateUserPass(req, res, next) {
    var user = req.userDoc;
    user.validatePassword(req.body.currentPassword, function (err, valid) {
        if (err)
            return next(err);
        if (!valid)
            return next(new error_1.UnauthorizedError('Authentication Failed'));
        user.password = req.body.newPassword;
        next();
    });
}
/* POST registration user data. */
router.post('/', auth_1.asRoot, createUser, returnUser);
/* GET user data. */
router.get('/:userid', findUserById, returnUser);
/* PUT user info update. */
router.put('/:userid', findUserById, auth_1.asLoginUser, updateUser, saveUser, returnUser);
/* PUT user password update. */
router.put('/:userid/password', findUserById, auth_1.asLoginUser, validateUserPass, saveUser, returnUser);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
