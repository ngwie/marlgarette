"use strict";
var sinon = require('sinon');
var request = require('supertest');
var app_1 = require('../app');
var user_1 = require('../models/user');
var agent = request.agent(app_1.default);
describe('Login Controller', function () {
    var stub;
    before(function () {
        stub = sinon.stub(user_1.default, 'findOne', function (query, callback) {
            callback(null, {
                id: 'user_id', name: 'user_name',
                validatePassword: function (password, next) { next(null, password === 'user_password'); }
            });
        });
    });
    after(function () {
        stub.restore();
    });
    afterEach(function (done) {
        agent.delete('/login').end(done);
    });
    describe('#GET user session', function () {
        it('should return session when session exist', function (done) {
            agent.post('/login')
                .send({ username: 'user_name', password: 'user_password' })
                .end(function (err, req) {
                if (err)
                    return done(err);
                agent.get('/login')
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .expect({ login: true, userid: 'user_id', username: 'user_name' })
                    .end(done);
            });
        });
        it('should return session login false when session not exist', function (done) {
            agent.get('/login')
                .expect(200)
                .expect('Content-Type', /json/)
                .expect({ login: false })
                .end(done);
        });
    });
    describe('#POST user login data', function () {
        it('should return session when login success', function (done) {
            agent.post('/login')
                .send({ username: 'user_name', password: 'user_password' })
                .expect(200)
                .expect('Content-Type', /json/)
                .expect({ login: true, userid: 'user_id', username: 'user_name' })
                .end(done);
        });
        it('should return 401 when login failed', function (done) {
            agent.post('/login')
                .send({ username: 'user_name', password: 'not_user_password' })
                .expect(401)
                .expect('Content-Type', /json/)
                .expect({
                message: 'Incorrect username or password',
                name: 'UnauthorizedError'
            })
                .end(done);
        });
    });
    describe('#DELETE user session', function () {
        it('should remove user session and return login false', function (done) {
            agent.post('/login')
                .send({ username: 'user_name', password: 'user_password' })
                .end(function (err, req) {
                if (err)
                    return done(err);
                agent.delete('/login')
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .expect({ login: false })
                    .end(done);
            });
        });
    });
});
