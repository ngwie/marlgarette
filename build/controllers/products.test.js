"use strict";
var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var request = require('supertest');
var passportStub = require('passport-stub');
var app_1 = require('../app');
var product_1 = require('../models/product');
var expect = chai.expect;
var fixtures = require('../test/fixtures/models');
describe('Products Controller', function () {
    before(function () {
        passportStub.install(app_1.default);
    });
    after(function () {
        passportStub.uninstall();
    });
    describe('#GET products', function () {
        var stubCount;
        var stubExec;
        var stubFind;
        beforeEach(function () {
            var query = product_1.default.find({});
            stubCount = sinon.stub(product_1.default, 'count', function (query, callback) {
                callback(null, 2);
            });
            stubExec = sinon.stub(query, 'exec', function (callback) {
                callback(null, _.map(fixtures.products, function (value) { return new product_1.default(value); }));
            });
            stubFind = sinon.stub(product_1.default, 'find').returns(query);
        });
        afterEach(function () {
            stubCount.restore();
            stubExec.restore();
            stubFind.restore();
        });
        it('should get all product', function (done) {
            request(app_1.default).get('/products')
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (res) {
                expect(res.body).contain.all.keys('products', 'page', 'pageCount', 'pageSize', 'totalCount');
                expect(res.body.products).to.have.lengthOf(2);
            })
                .end(done);
        });
    });
    describe('#POST a new product', function () {
        var stubSave;
        var stubConstructor;
        beforeEach(function () {
            var product = new product_1.default();
            stubSave = sinon.stub(product, 'save', function (callback) {
                callback(null, new product_1.default(fixtures.products[0]));
            });
            stubConstructor = sinon.stub(require('../models/product'), 'default').returns(product);
        });
        afterEach(function () {
            stubSave.restore();
            stubConstructor.restore();
            passportStub.logout();
        });
        it('should return forbidden when user is not an admin', function (done) {
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' });
            request(app_1.default).post('/products')
                .send(fixtures.products[0])
                .expect(403)
                .expect('Content-Type', /json/)
                .expect(function (res) {
                expect(res.body).contain.all.keys('message');
            })
                .end(done);
        });
        it('should create new product', function (done) {
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' });
            request(app_1.default).post('/products')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (res) {
                expect(res.body).contain.all.keys('product');
                expect(stubConstructor.calledWithNew()).to.be.true;
                expect(stubSave.calledOnce).to.be.true;
            })
                .end(done);
        });
    });
    describe('#GET a product', function () {
        var stub;
        beforeEach(function () {
            stub = sinon.stub(product_1.default, 'findById', function (productId, callback) {
                callback(null, productId === 'my_product_id' ? new product_1.default(fixtures.products[0]) : null);
            });
        });
        afterEach(function () {
            stub.restore();
        });
        it('should return product when product is exist', function (done) {
            request(app_1.default).get('/products/my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (res) {
                expect(res.body).contain.all.keys('product');
                expect(stub.calledOnce).to.be.true;
                expect(stub.args[0][0]).to.equal('my_product_id');
                expect(stub.args[0][1]).to.be.a('function');
            })
                .end(done);
        });
        it('should return not found when product is not exist', function (done) {
            request(app_1.default).get('/products/not_my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(404)
                .expect(function (res) {
                expect(res.body).contain.all.keys('name', 'message');
                expect(stub.calledOnce).to.be.true;
                expect(stub.args[0][0]).to.equal('not_my_product_id');
                expect(stub.args[0][1]).to.be.a('function');
            })
                .end(done);
        });
    });
    describe('#PUT a product update', function () {
        var stubFind;
        var stubSave;
        beforeEach(function () {
            var product = new product_1.default(fixtures.products[0]);
            stubSave = sinon.stub(product, 'save', function (callback) {
                callback(null, product);
            });
            stubFind = sinon.stub(product_1.default, 'findById', function (productId, callback) {
                callback(null, productId === 'my_product_id' ? product : null);
            });
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' });
        });
        afterEach(function () {
            stubFind.restore();
            stubSave.restore();
            passportStub.logout();
        });
        it('should update and return product when product is exist', function (done) {
            request(app_1.default).put('/products/my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (res) {
                expect(stubFind.calledOnce).to.be.true;
                expect(stubFind.args[0][0]).to.equal('my_product_id');
                expect(stubFind.args[0][1]).to.be.a('function');
                expect(stubSave.calledOnce).to.be.true;
                expect(stubSave.args[0][0]).to.be.a('function');
            })
                .end(done);
        });
        it('should return not found when product is not exist', function (done) {
            request(app_1.default).put('/products/not_my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(404)
                .expect(function (res) {
                expect(stubFind.calledOnce).to.be.true;
                expect(stubFind.args[0][0]).to.equal('not_my_product_id');
                expect(stubFind.args[0][1]).to.be.a('function');
                expect(stubSave.called).to.be.false;
            })
                .end(done);
        });
        it('should return forbidden when user is not an admin', function (done) {
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' });
            request(app_1.default).put('/products/not_my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(403)
                .expect(function (res) {
                expect(res.body).contain.all.keys('message');
            })
                .end(done);
        });
    });
    describe('#DELETE a product', function () {
        var stub;
        beforeEach(function () {
            stub = sinon.stub(product_1.default, 'findByIdAndRemove', function (productId, callback) {
                callback(null, productId === 'my_product_id' ? new product_1.default(fixtures.products[0]) : null);
            });
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'admin' });
        });
        afterEach(function () {
            stub.restore();
            passportStub.logout();
        });
        it('should delete and return product when product is exist', function (done) {
            request(app_1.default).delete('/products/my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (res) {
                expect(stub.calledOnce).to.be.true;
                expect(stub.args[0][0]).to.equal('my_product_id');
                expect(stub.args[0][1]).to.be.a('function');
            })
                .end(done);
        });
        it('should return not found when product is not exist', function (done) {
            request(app_1.default).delete('/products/not_my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(404)
                .expect(function (res) {
                expect(stub.calledOnce).to.be.true;
                expect(stub.args[0][0]).to.equal('not_my_product_id');
                expect(stub.args[0][1]).to.be.a('function');
            })
                .end(done);
        });
        it('should return forbidden when user is not an admin', function (done) {
            passportStub.login({ id: 'user_id', name: 'user_name', authority: 'user' });
            request(app_1.default).delete('/products/my_product_id')
                .send(fixtures.products[0])
                .expect('Content-Type', /json/)
                .expect(403)
                .expect(function (res) {
                expect(res.body).contain.all.keys('message');
            })
                .end(done);
        });
    });
    describe('#POST a product picture', function () { });
});
