"use strict";
var express = require('express');
var router = express.Router();
/* GET home page. */
router.get(/^\/(?!admin).*/, function (req, res, next) {
    if (!req.accepts('text/html'))
        return next();
    res.render('index');
});
/* GET admin home page. */
router.get(/^\/admin.*/, function (req, res, next) {
    if (!req.accepts('text/html'))
        return next();
    res.render('admin');
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
