"use strict";
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'name is required']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'email is required'],
        match: [/\S+@\S+\.\S+/, 'email is invalid']
    },
    password: {
        type: String,
        required: [true, 'password is required']
    },
    authority: {
        type: String,
        default: 'customer',
        required: [true, 'authority is required'],
        enum: {
            values: ['customer', 'admin', 'root'],
            message: 'invalid authority value'
        }
    }
}, {
    timestamps: true
});
userSchema.pre('save', function (next) {
    var _this = this;
    if (!this.isModified('password'))
        return next();
    bcrypt.hash(this.password, 10, function (err, hash) {
        if (err)
            return next(err);
        _this.password = hash;
        next();
    });
});
userSchema.method('validatePassword', function (password, next) {
    bcrypt.compare(password, this.password, next);
});
var User = mongoose.model('User', userSchema);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = User;
