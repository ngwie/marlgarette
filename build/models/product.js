"use strict";
var mongoose = require('mongoose');
var priceSchema = new mongoose.Schema({
    currency: {
        type: String,
        default: 'IDR',
        required: [true, 'price currency is required'],
        enum: {
            values: ['IDR', 'USD'],
            message: 'currency is not supported'
        }
    },
    value: {
        type: Number,
        required: [true, 'price value is required']
    }
});
var productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'name is required']
    },
    brand: {
        type: String,
        required: [true, 'brand is required']
    },
    displayed: {
        type: Boolean,
        default: false,
        required: [true, 'displayed is required']
    },
    prices: {
        type: [priceSchema],
        required: [true, 'prices is required']
    },
    sizes: Array,
    quantity: Object,
    flags: [String],
    pictures: Array,
    description: String,
}, {
    timestamps: true
});
var Product = mongoose.model('Product', productSchema);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Product;
