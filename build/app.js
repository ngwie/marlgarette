"use strict";
var path = require('path');
var config = require('config');
var express = require('express');
var mongoose = require('mongoose');
var appRoot = require('app-root-path');
var bodyParser = require('body-parser');
var session = require('express-session');
var connectMongo = require('connect-mongo');
var cookieParser = require('cookie-parser');
var logger_1 = require('./lib/logger');
var passport_1 = require('./lib/passport');
var database_1 = require('./lib/database');
var error_handler_1 = require('./lib/middleware/error-handler');
var health_1 = require('./controllers/health');
var controllers_1 = require('./controllers');
var users_1 = require('./controllers/users');
var login_1 = require('./controllers/login');
var products_1 = require('./controllers/products');
var error_1 = require('./lib/error');
var app = express();
var MongoStore = connectMongo(session);
// view engine setup
app.set('views', path.join(appRoot.path, 'views'));
app.set('view engine', 'ejs');
app.set('config', config.get('app'));
// setup database
database_1.default(config.get('database'));
// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'random_string_key',
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
app.use(passport_1.default.initialize());
app.use(passport_1.default.session());
if (app.get('env') === 'development') {
    app.use(require('less-middleware')(path.join(__dirname, 'public')));
}
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(app.get('config').staticDir, 'images')));
// setup logger
logger_1.default.level(app.get('env') === 'test' ? 'error' : 'debug');
app.use(logger_1.requestLogger);
app.use('/health', health_1.default);
app.use('/users', users_1.default);
app.use('/login', login_1.default);
app.use('/products', products_1.default);
app.use('/', controllers_1.default);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(new error_1.NotFoundError('Path Not Found'));
});
// error handler
app.use(error_handler_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = app;
