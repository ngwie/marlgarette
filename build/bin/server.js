/**
 * Module dependencies.
 */
"use strict";
var cluster = require('cluster');
var logger_1 = require('../lib/logger');
var IS_PRODUCTION = process.env.NODE_ENV == 'production';
var STOP_SIGNALS = [
    'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
    'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
];
var stopping = false;
cluster.on('disconnect', function (worker) {
    if (IS_PRODUCTION) {
        if (!stopping) {
            cluster.fork();
        }
    }
    else {
        process.exit(1);
    }
});
if (cluster.isMaster) {
    var workerCount = process.env.NODE_CLUSTER_WORKERS || 4;
    logger_1.default.info("Starting " + workerCount + " workers...");
    for (var i = 0; i < workerCount; i++) {
        cluster.fork();
    }
    if (IS_PRODUCTION) {
        STOP_SIGNALS.forEach(function (signal) {
            process.on(signal, function () {
                logger_1.default.info("Got " + signal + ", stopping workers...");
                stopping = true;
                cluster.disconnect(function () {
                    logger_1.default.info('All workers stopped, exiting.');
                    process.exit(0);
                });
            });
        });
    }
}
else {
    require('./www');
}
