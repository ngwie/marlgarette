/**
 * Module dependencies.
 */
"use strict";
var http = require('http');
var config = require('config');
var app_1 = require('../app');
var logger_1 = require('../lib/logger');
var database_1 = require('../lib/database');
/**
 * Get port from environment from Express store.
 */
var _a = config.get('server'), port = _a.port, host = _a.host;
/**
 * Create HTTP server.
 */
var server = http.createServer(app_1.default);
/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, host);
server.on('error', onError);
server.on('close', onClose);
server.on('listening', onListening);
/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}
/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            logger_1.default.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger_1.default.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}
/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    logger_1.default.info('Server listening on ' + bind);
}
/**
 * Event listener for HTTP server "close" event.
 */
function onClose() {
    database_1.closeConnection();
    logger_1.default.info("Server on " + process.pid + " is closing");
}
