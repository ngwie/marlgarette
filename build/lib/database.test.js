"use strict";
var chai = require('chai');
var db = require('../test/helpers/database');
var product_1 = require('../models/product');
var expect = chai.expect;
describe('DB Connetion', function () {
    before(function (done) {
        db.connect(done);
    });
    describe('#promise', function () {
        it('should using native promise', function () {
            var query = product_1.default.findOne({ id: 'test_id' });
            expect(query.exec()).to.be.instanceof(global.Promise);
        });
    });
});
