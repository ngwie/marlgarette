"use strict";
var passport = require('passport');
var passport_local_1 = require('passport-local');
var error_1 = require('./error');
var user_1 = require('../models/user');
passport.use(new passport_local_1.Strategy(function (username, password, done) {
    user_1.default.findOne({ email: username }, function (err, user) {
        if (err)
            return done(err);
        if (!user)
            return done(null, false, { message: 'Incorrect username.' });
        user.validatePassword(password, function (err, res) {
            if (err)
                return done(err);
            if (res) {
                done(null, user);
            }
            else {
                done(null, false, { message: 'Incorrect password.' });
            }
        });
    });
}));
passport.serializeUser(function (user, done) {
    done(null, {
        id: user.id,
        name: user.name,
        authority: user.authority
    });
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
function authenticate(req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err)
            return next(err);
        if (!user)
            return next(new error_1.UnauthorizedError('Incorrect username or password'));
        req.logIn(user, next);
    })(req, res, next);
}
exports.authenticate = authenticate;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = passport;
