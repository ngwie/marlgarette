"use strict";
var bunyan = require('bunyan');
var uuid = require('node-uuid');
var _ = require('lodash');
var logger = bunyan.createLogger({
    name: 'marlgarette',
    level: 'debug',
    serializers: {
        req: bunyan.stdSerializers.req,
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err
    }
});
function filterReq(req) {
    var obj = _.cloneDeep(req);
    obj.headers = _.omit(req.headers, 'cookie');
    return obj;
}
function requestLogger(req, res, next) {
    var start = Date.now();
    var requestId = req.headers['x-request-id'] || uuid.v4();
    req.logger = logger.child({ 'reqId': requestId });
    res.on('finish', function () {
        req.logger.info({
            duration: Date.now() - start,
            req: filterReq(req),
            res: res
        }, 'request finish');
    });
    res.on('close', function () {
        req.logger.warn({
            duration: Date.now() - start,
            req: filterReq(req),
            res: res
        }, 'request socket closed');
    });
    next();
}
exports.requestLogger = requestLogger;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = logger;
