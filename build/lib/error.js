"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// Bad Request error 400
var BadRequestError = (function (_super) {
    __extends(BadRequestError, _super);
    function BadRequestError(message) {
        _super.call(this, message);
        this.message = message;
        this.name = 'BadRequestError';
    }
    return BadRequestError;
}(Error));
exports.BadRequestError = BadRequestError;
// Unauthorized error 401
var UnauthorizedError = (function (_super) {
    __extends(UnauthorizedError, _super);
    function UnauthorizedError(message) {
        _super.call(this, message);
        this.message = message;
        this.name = 'UnauthorizedError';
    }
    return UnauthorizedError;
}(Error));
exports.UnauthorizedError = UnauthorizedError;
// Forbidden error 403
var ForbiddenError = (function (_super) {
    __extends(ForbiddenError, _super);
    function ForbiddenError(message) {
        _super.call(this, message);
        this.message = message;
        this.name = 'ForbiddenError';
    }
    return ForbiddenError;
}(Error));
exports.ForbiddenError = ForbiddenError;
// Not found error 404
var NotFoundError = (function (_super) {
    __extends(NotFoundError, _super);
    function NotFoundError(message) {
        _super.call(this, message);
        this.message = message;
        this.name = 'NotFoundError';
    }
    return NotFoundError;
}(Error));
exports.NotFoundError = NotFoundError;
// Server error 500
var ServerError = (function (_super) {
    __extends(ServerError, _super);
    function ServerError(message) {
        _super.call(this, message);
        this.message = message;
        this.name = 'ServerError';
    }
    return ServerError;
}(Error));
exports.ServerError = ServerError;
