"use strict";
var mongoose = require('mongoose');
var logger_1 = require('./logger');
// set mongoose promise to use native promises
require('mongoose').Promise = global.Promise;
function default_1(config, cb) {
    if (mongoose.connection.readyState !== 0) {
        if (cb)
            cb();
        return;
    }
    var uri = config.host + config.database;
    mongoose.connect(uri);
    mongoose.connection.on('error', function (error) {
        logger_1.default.error('Database connection error:', error);
    });
    mongoose.connection.once('open', function () {
        if (cb)
            cb();
        logger_1.default.info('Database connection open');
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
function closeConnection() {
    mongoose.connection.close();
}
exports.closeConnection = closeConnection;
