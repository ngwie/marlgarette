"use strict";
var error_1 = require('../error');
var UNAUTHORIZED_ERR_MSG = 'Authentication credentials were missing or incorrect.';
var FORBIDDEN_ERR_MSG = 'The request is understood, but it has been refused or access is not allowed.';
var SERVER_ERR_MSG = 'User data should be provided';
function asAdmin(req, res, next) {
    if (!req.user) {
        return next(new error_1.UnauthorizedError(UNAUTHORIZED_ERR_MSG));
    }
    if (req.user.authority !== 'admin') {
        return next(new error_1.ForbiddenError(FORBIDDEN_ERR_MSG));
    }
    next();
}
exports.asAdmin = asAdmin;
function asRoot(req, res, next) {
    if (!req.user) {
        return next(new error_1.UnauthorizedError(UNAUTHORIZED_ERR_MSG));
    }
    if (req.user.authority !== 'root') {
        return next(new error_1.ForbiddenError(FORBIDDEN_ERR_MSG));
    }
    next();
}
exports.asRoot = asRoot;
function asLoginUser(req, res, next) {
    if (!req.user) {
        return next(new error_1.UnauthorizedError(UNAUTHORIZED_ERR_MSG));
    }
    if (!req.userDoc) {
        return next(new error_1.ServerError(SERVER_ERR_MSG));
    }
    if (req.userDoc._id.toHexString() !== req.user.id) {
        return next(new error_1.ForbiddenError(FORBIDDEN_ERR_MSG));
    }
    next();
}
exports.asLoginUser = asLoginUser;
