"use strict";
function pageQuery(req, res, next) {
    var page = req.query.page = parseInt(req.query.page) || 1;
    var pageSize = req.query.pagesize = parseInt(req.query.pagesize) || 9;
    req.query.limit = pageSize;
    req.query.skip = pageSize * (page - 1);
    next();
}
exports.pageQuery = pageQuery;
function productQuery(req, res, next) {
    var query = {};
    var flag = req.query.flag;
    if (flag)
        query.flags = flag;
    req.query.productQuery = query;
    pageQuery(req, res, next);
}
exports.productQuery = productQuery;
