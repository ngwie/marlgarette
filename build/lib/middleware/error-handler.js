"use strict";
var _ = require('lodash');
var logger_1 = require('../logger');
function default_1(err, req, res, next) {
    logger_1.default.warn(err);
    var errorCode;
    if (err.name === 'BadRequestError' ||
        _.includes(['ValidationError', 'CastError'], err.name)) {
        errorCode = 400;
    }
    else if (err.name === 'UnauthorizedError') {
        errorCode = 401;
    }
    else if (err.name === 'ForbiddenError') {
        errorCode = 403;
    }
    else if (err.name === 'NotFoundError') {
        errorCode = 404;
    }
    else {
        errorCode = 500;
    }
    res.status(errorCode);
    res.json({
        name: err.name,
        message: err.message
    });
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
