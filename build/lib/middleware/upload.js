"use strict";
var _ = require('lodash');
var path = require('path');
var mime = require('mime');
var multer = require('multer');
var randomstring = require('randomstring');
exports.productPicture = multer({
    fileFilter: function (req, file, cb) {
        var acceptedType = ['image/jpeg', 'image/png'];
        cb(null, _.includes(acceptedType, file.mimetype));
    },
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            var config = req.app.get('config');
            cb(null, path.join(config.staticDir, config.productImageDir));
        },
        filename: function (req, file, cb) {
            var name = _.kebabCase(req.productDoc.name) +
                '-' + randomstring.generate(12) +
                '.' + mime.extension(file.mimetype);
            cb(null, name);
        }
    })
});
