
import * as factory from 'factory-girl';

import User from '../../models/user';

factory.define('user', User, {
  name: 'Miller .Jr',
  email: 'miller.jr@gmail.com',
  password: 'test',
  authority: 'admin'
});

export default factory;