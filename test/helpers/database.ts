import * as mongoose from 'mongoose'
import * as async from 'async'
import * as config from 'config'
import database, { IDatabaseConfig } from '../../lib/database'

export function connect(next) {

  if (mongoose.connection.readyState !== 0) return next()

  const databaseConfig = config.get<IDatabaseConfig>('database')

  database(databaseConfig, next)
}

export function getDB() {
  return mongoose.connection.db
}

export function drop(done) {
  mongoose.connection.db.dropDatabase(done);
}

export function fixtures(data, done) {
  /*
  var db = state.db
  if (!db) {
    return done(new Error('Missing database connection.'))
  }

  var names = Object.keys(data.collections)
  async.each(name, function(name, cb) {
    db.createCollection(name, function(err, collection) {
      if (err) return cb(err)
      collection.insert(data.collections[name], cb)
    })
  }, done)
  */
}